#include "opencv2/core/cuda.hpp"

int main()
{
    cv::Mat::setDefaultAllocator(cv::cuda::HostMem::getAllocator(cv::cuda::HostMem::AllocType::SHARED));

    return 0;
}
